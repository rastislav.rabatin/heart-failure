#!/usr/bin/env python
import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

from sklearn.compose import ColumnTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import VotingClassifier
from sklearn.metrics import classification_report, f1_score
from sklearn.model_selection import GridSearchCV

from joblib import dump, load


NUMERIC_FEATURES = ["age", "creatinine_phosphokinase", "serum_sodium", "time", "platelets",
        "ejection_fraction", "serum_creatinine"]
CATEGORICAL_FEATURES = ["anaemia", "high_blood_pressure", "diabetes", "sex", "smoking"]



def train_model(model, X_train, y_train, X_test, y_test):
    model = model.fit(X_train, y_train)
    y_train_pred = model.predict(X_train)
    y_test_pred = model.predict(X_test)
    print(classification_report(y_train, y_train_pred))
    print(classification_report(y_test, y_test_pred))
    return model


def main():
    df = pd.read_csv("heart_failure_clinical_records_dataset.csv")

    n_rows = df.shape[0]
    # shuffle rows
    df = df.sample(n_rows)
    n_train = int(n_rows * 0.8)
    n_test = n_rows - n_train
    train_df = df.head(n_train)
    test_df = df.tail(n_test)
    feats = df.columns.tolist()
    target = "DEATH_EVENT"
    feats.remove(target)

    X_train = train_df[feats]
    y_train = train_df[target]

    X_test = test_df[feats]
    y_test = test_df[target]

    print(feats)
    print(train_df.head(1).to_dict("records"))

    scale = ColumnTransformer(transformers=[
        ('scale', StandardScaler(), NUMERIC_FEATURES)],
        remainder='passthrough'
    )

    log_reg = Pipeline(steps=[
        ('scale', scale),
        ('classify',
         GridSearchCV(LogisticRegression(),
                      param_grid={"C": [0.001, 0.01, 0.1, 1, 10, 100]},
                      refit=True, scoring="f1")
        )
    ])

    svm = Pipeline(steps=[
        ('scale', scale),
        ('classify',
         GridSearchCV(SVC(),
                      param_grid={"C": [0.001, 0.01, 0.1, 1, 10], "kernel": ["linear", "rbf"]},
                      refit=True, scoring="f1")
        )
    ])

    tree = Pipeline(steps=[
        ('scale', scale),
        ('classify', GridSearchCV(DecisionTreeClassifier(),
                                  param_grid={"max_depth": [2, 4, 5, 6, 7, 8, 9, 10, 12]},
                                  refit=True, scoring="f1"))
    ])

    models = {"tree": tree, "svm": svm, "log_reg": log_reg}
    for name, model_obj in models.items():
        print(name)
        trained_model = train_model(model_obj, X_train, y_train, X_test, y_test)
        dump(trained_model, f"{name}.joblib")


if __name__ == "__main__":
    main()
